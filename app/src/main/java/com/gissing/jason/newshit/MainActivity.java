package com.gissing.jason.newshit;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.HashMap;


public class MainActivity extends ActionBarActivity {

    private static Context context;

    private RecyclerView mRecyclerView;
    private FeedAdapter mAdapter;
    private ArrayList<HashMap<String, String>> myDataset;
    private RSSGrabber mRSSGrabber;
    private ProgressDialog mProgDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        MainActivity.context = getApplicationContext();

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mRecyclerView = (RecyclerView) findViewById(R.id.feed_recycler_view);

        // Initialise Progress dialog:
        mProgDialog = new ProgressDialog(this);
        mProgDialog.setIndeterminate(true);
        mProgDialog.setTitle(R.string.update_feed_progress_title);
        mProgDialog.setCancelable(false);
        mProgDialog.setMessage(getResources().getString(R.string.update_feed_progress_message));


        // Initialise List adapter:
        myDataset = new ArrayList<HashMap<String, String>>();
        mAdapter = new FeedAdapter(myDataset, this);
        mRecyclerView.setAdapter(mAdapter);

        // improve performance if you know that changes in content
        // do not change the size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager (only option at this point?)
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);

        startRefresh();
    }

    @Override
    protected void onStop() {
        super.onStop();

        mProgDialog.dismiss();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_refresh) {
            startRefresh();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    private void startRefresh() {
        mProgDialog.show();
        mRSSGrabber = new RSSGrabber(this);
        mRSSGrabber.execute();
    }


    private void stopRefresh() {
        if (mRSSGrabber != null) {
            mRSSGrabber.cancel(true);
            mRSSGrabber = null;
        }
        mProgDialog.dismiss();
    }


    public void refreshComplete(ArrayList<HashMap<String, String>> newsItems) {
        stopRefresh();
        // If result is null (as opposed to an empty list), there was a network error.
        if (newsItems == null)
            showMessage(R.string.network_error_message,R.string.network_error_title);
        else
            updateFeed(newsItems);
    }


    public void updateFeed(ArrayList<HashMap<String, String>> data) {
        myDataset = data;
        mAdapter.updateData(myDataset);
    }


    public void showMessage(int messageID, int titleID) {
         new AlertDialog.Builder(this)
                 .setTitle(getResources().getString(titleID))
                 .setMessage(getResources().getString(messageID))
                 .setNeutralButton("OK", null)
                 .show();
    }


    public void loadStory(String storyUrl) {
        Intent storyIntent = new Intent(this, StoryActivity.class);
        storyIntent.putExtra(RSSGrabber.ITEM_LINK, storyUrl);
        startActivity(storyIntent);
    }


    public static boolean shouldHireAuthor() {
        return true;
    }
}
