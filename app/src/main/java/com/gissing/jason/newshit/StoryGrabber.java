package com.gissing.jason.newshit;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.net.URL;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Jason on 03/08/2014.
 */
public class StoryGrabber extends AsyncTask<String, String, Integer> {

    private static final int SUCCESS_CODE = 0;
    private static final int ERROR_CODE_CONNECTION = 1; // Unable to fetch the story's html content.
    private static final int ERROR_CODE_FORMAT_ISSUE = 2; // Unable to parse the story;s html document.

    private static final String CLASS_STORY_BODY = "story-body"; // Only currently support stories containing a "story-body" div.

    private Context mContext;
    private ProgressDialog mProgDialog;
    private String mURL;

    private List<String> mElementBlacklist; // A list of element types to ignore when parsing HTML
    private List<String> mClassWhitelist; // A list of classnames - unless a div contains any of these, ignore it. Not used yet!


    public StoryGrabber(Context context) {
        mContext = context;
        initialiseBlacklists();
    }


    private void initialiseBlacklists() {
        String elemBlacklist[] = {
                "script",
                "div",
                "form"
        };
        mElementBlacklist = Arrays.asList(elemBlacklist);

        String classWhitelist[] = {
                "caption full-width"
        };
        mClassWhitelist = Arrays.asList(classWhitelist);
    }


    @Override
    protected Integer doInBackground(String... urls) {
        mURL = urls[0];
        try {
            Document doc = Jsoup.parse(new URL(mURL), 30000);
            if (doc == null)
                return ERROR_CODE_CONNECTION;

            Element storyElem = doc.getElementsByClass(CLASS_STORY_BODY).get(0);
            addSupportedElements(storyElem);

        }
        catch (Exception e) {
            Log.e("Jason", e.toString());
            return ERROR_CODE_FORMAT_ISSUE;
        }
        return SUCCESS_CODE;
    }


    private void addSupportedElements(Element rootElement) throws Exception {
        Elements elements = rootElement.children();
        if (elements == null)
            return;

        for (Element elem : elements) {
            String tagName = elem.tagName();
            if (tagName.equalsIgnoreCase("img")) { // Special handling for img tags (need to get content from attribute)
                publishProgress(tagName, elem.attr("src"), elem.className());
            }
            else if (!mElementBlacklist.contains(tagName)) {
                publishProgress(tagName, elem.text(), elem.className());
            }
            else if (mClassWhitelist.contains(elem.className())) {
                addSupportedElements(elem);
            }
        }
    }


    @Override
    protected void onPreExecute() {
        mProgDialog = new ProgressDialog(mContext);
        mProgDialog.setIndeterminate(true);
        mProgDialog.setTitle(R.string.update_feed_progress_title);
        mProgDialog.setMessage("Retrieving News Story");
        mProgDialog.setCancelable(false);
        mProgDialog.show();
    }


    /**
     *
     * @param values param0 = tag name. param1 = content. param2 = className(s)
     */
    @Override
    protected void onProgressUpdate(String... values) {
        ((StoryActivity) mContext).addStoryBlock(values[0], values[1], values[2]);
    }


    @Override
    protected void onPostExecute(Integer resultCode) {
        mProgDialog.dismiss();
        mProgDialog = null;
        String errorMessage = "";

        switch (resultCode) {
            case SUCCESS_CODE:
                ((StoryActivity) mContext).notifyDataChanged();
                return;
            case ERROR_CODE_CONNECTION:
                errorMessage = "Unable to fetch story";
                break;
            case ERROR_CODE_FORMAT_ISSUE:
                errorMessage = "Story format not supported";
                // Fall back to opening story in default browser:
                ((StoryActivity) mContext).loadStoryInBrowser();
                break;
        }
        // If it has fallen through to here, there has been an error - Add a row to the story ListView to explain.
        ((StoryActivity) mContext).addStoryBlock("h1", errorMessage, null);
        ((StoryActivity) mContext).notifyDataChanged();
    }
}
