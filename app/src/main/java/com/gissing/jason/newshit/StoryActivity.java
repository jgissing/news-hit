package com.gissing.jason.newshit;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;


public class StoryActivity extends ActionBarActivity {

    public static final int ROW_TYPE_DEFAULT = 0;
    public static final int ROW_TYPE_HEADER = 1;
    public static final int ROW_TYPE_SUBTITLE = 2;
    public static final int ROW_TYPE_SPAN = 3;
    public static final int ROW_TYPE_IMAGE = 4;
    public static final int ROW_TYPE_COUNT = ROW_TYPE_IMAGE + 1;

    private StoryListAdapter mAdapter;
    private String mUrl;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_story);

        final ActionBar actionBar = getSupportActionBar();
        if(actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
            actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        }

        mUrl = getIntent().getExtras().getString(RSSGrabber.ITEM_LINK);

        mAdapter = new StoryListAdapter(this);
        ListView lv = (ListView) findViewById(R.id.story_list_view);
        lv.setAdapter(mAdapter);

        new StoryGrabber(this).execute(mUrl);
    }


    /**
     * Adds a row to the list adapter. When getting the view, the list adapter will vary the appearance based on tagType & className.
     * @param tagType
     * @param content
     * @param className
     */
    public void addStoryBlock(String tagType, String content, String className) {
        HashMap<String, String> storyBlock = new HashMap<String, String>(3);
        storyBlock.put("tag",tagType);
        storyBlock.put("content", content);
        storyBlock.put("class", className);
        mAdapter.addData(storyBlock);
    }


    // Called once the story html has finished being parsed
    public void notifyDataChanged() {
        // If no rows have been parsed, display an error message and fall back to opening in default browser.
        if (mAdapter.getCount() == 0) {
            addStoryBlock("h1", getResources().getString(R.string.error_showing_story), null);
            loadStoryInBrowser();
        }

        mAdapter.notifyDataSetChanged();
    }


    public void loadStoryInBrowser() {
        // Fall back to opening story in default browser:
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(mUrl));
        startActivity(browserIntent);
    }


    private class StoryListAdapter extends ArrayAdapter<HashMap<String, String>> {

        private ArrayList<HashMap<String, String>> mData;
        private Context mContext;
        private LayoutInflater mInflater;


        public StoryListAdapter(Context context) {
            super(context, R.layout.story_default_text_row);

            mContext = context;
            mData = new ArrayList<HashMap<String, String>>();
            mInflater = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }


        public void addData(HashMap<String, String> newData) {
            mData.add(newData);
        }


        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder holder = null;
            int type = getItemViewType(position);

            if (convertView == null) {
                holder = new ViewHolder();
                // Find the appropriate row layout based on the 'type':
                switch (type) {
                    case ROW_TYPE_DEFAULT:
                        convertView = mInflater.inflate(R.layout.story_default_text_row, null);
                        holder.contentView = convertView.findViewById(R.id.story_row_text_default);
                        break;
                    case ROW_TYPE_HEADER:
                        convertView = mInflater.inflate(R.layout.story_header_text_row, null);
                        holder.contentView = convertView.findViewById(R.id.story_row_header);
                        break;
                    case ROW_TYPE_SUBTITLE:
                        convertView = mInflater.inflate(R.layout.story_subheader_text_row, null);
                        holder.contentView = convertView.findViewById(R.id.story_row_subheader);
                        break;
                    case ROW_TYPE_SPAN:
                        convertView = mInflater.inflate(R.layout.story_span_row, null);
                        holder.contentView = convertView.findViewById(R.id.story_row_span);
                        break;
                    case ROW_TYPE_IMAGE:
                        convertView = mInflater.inflate(R.layout.story_image_row, null);
                        holder.contentView = convertView.findViewById(R.id.story_row_image);
                        break;
                }
                convertView.setTag(holder);
            } else {
                holder = (ViewHolder)convertView.getTag();
            }

            // Set the content of the inflated row:
            switch (type) {
                case ROW_TYPE_DEFAULT:
                case ROW_TYPE_HEADER:
                case ROW_TYPE_SUBTITLE:
                case ROW_TYPE_SPAN:
                    ((TextView) holder.contentView).setText(mData.get(position).get("content"));
                    break;
                case ROW_TYPE_IMAGE:
                    Picasso.with(mContext)
                            .load(mData.get(position).get("content"))
                            .into( (ImageView) holder.contentView);
                    break;
            }

            return convertView;
        }


        @Override
        public int getCount() {
            return mData.size();
        }


        @Override
        public int getViewTypeCount() {
            return ROW_TYPE_COUNT;
        }


        @Override
        public int getItemViewType(int position) {
            String tagName = mData.get(position).get("tag");

            if (tagName.length() == 2 && tagName.charAt(0) == 'h')
                return ROW_TYPE_HEADER;

            else if (tagName.equalsIgnoreCase("span")) {
                if (mData.get(position).get("class").equalsIgnoreCase("cross-head"))
                    return ROW_TYPE_SUBTITLE;
                return ROW_TYPE_SPAN;
            }

            else if (tagName.equalsIgnoreCase("img"))
                return ROW_TYPE_IMAGE;

            return ROW_TYPE_DEFAULT;
        }
    }


    /**
     * ViewHolder class for StoryListAdapter, to reduce calls to findViewById().
     * Contains a single "contentView" property - cast to the view type for the specific row when reading.
     */
    public static class ViewHolder {
        public View contentView; // The single view used for the row's content.
    }
}
