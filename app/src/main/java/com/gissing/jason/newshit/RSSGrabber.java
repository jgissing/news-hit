package com.gissing.jason.newshit;

import android.content.Context;
import android.os.AsyncTask;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Jason on 02/08/2014.
 */
public class RSSGrabber extends AsyncTask<Void, Void, ArrayList<HashMap<String, String>>> {

    // Strings which match the xml tag names used by the BBC's RSS feed:
    public static final String ITEM_CONTAINER = "item";
    public static final String ITEM_TITLE = "title";
    public static final String ITEM_DESC = "description";
    public static final String ITEM_LINK = "link";
    public static final String ITEM_PUBLISHED = "pubDate";
    public static final String ITEM_IMAGE = "media:thumbnail";

    private static final String FEED_URL = "http://feeds.bbci.co.uk/news/rss.xml";

    private Context mContext;


    public RSSGrabber(Context context) {
        mContext = context;
    }


    @Override
    protected ArrayList<HashMap<String, String>> doInBackground(Void... params) {

        ArrayList<HashMap<String, String>> newsItemsList = new ArrayList<HashMap<String, String>>();

        try {
            URL url = new URL(FEED_URL);
            InputStream inputStream;
            try {
                inputStream = url.openConnection().getInputStream();
            }
            catch (IOException e) {
                return null;
            }

            XmlPullParserFactory factory = XmlPullParserFactory.newInstance();
            XmlPullParser xpp = factory.newPullParser();
            xpp.setInput(inputStream, "UTF_8");

            int eventType = xpp.getEventType();
            HashMap<String, String> currentItem = null;

            while (eventType != XmlPullParser.END_DOCUMENT) {
                if (eventType == XmlPullParser.START_TAG) {
                    String name = xpp.getName();

                    // Only care about tags which are inside <item> elements (currentItem is not null)
                    if (currentItem != null) {
                        if (name.equalsIgnoreCase(ITEM_IMAGE)) { // thumbnail elements hold their content in attributes
                            // Append image size to the name:
                            name += ":" + xpp.getAttributeValue(null, "width");
                            name += "x" + xpp.getAttributeValue(null, "height");
                            currentItem.put(name, xpp.getAttributeValue(null, "url"));
                        }
                        else
                            currentItem.put(name, xpp.nextText());
                    }

                    if (name.equalsIgnoreCase(ITEM_CONTAINER)) {
                        currentItem = new HashMap<String, String>();
                    }
                }
                else if(eventType==XmlPullParser.END_TAG && xpp.getName().equalsIgnoreCase(ITEM_CONTAINER)) { // Leaving an <item> tag
                    newsItemsList.add(currentItem);
                    currentItem = null; // So currentItem is only non-null while we're inside an item tag.
                }

                eventType = xpp.next(); // Move to next element
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return newsItemsList;
    }


    @Override
    protected void onPostExecute(ArrayList<HashMap<String, String>> newsItems) {
        ((MainActivity)mContext).refreshComplete(newsItems);
    }
}
