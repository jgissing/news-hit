package com.gissing.jason.newshit;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Jason on 02/08/2014.
 * Adapter for use with RecyclerView. Uses Card Views.
 */
public class FeedAdapter extends RecyclerView.Adapter<FeedAdapter.ViewHolder> {
    private ArrayList<HashMap<String, String>> mDataset;
    private short mColCount = 1;
    private Context mContext;


    public FeedAdapter(ArrayList<HashMap<String, String>> myDataset, Context context) {
        mContext = context;

        // Work out colCount: TODO: Find a better way of doing this
        DisplayMetrics displayMetrics = mContext.getResources().getDisplayMetrics();
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        if (dpWidth >= 400f)
            mColCount = 2;

        mDataset = myDataset;
    }


    public void updateData(ArrayList<HashMap<String, String>> myDataset) {
        mDataset = myDataset;
        notifyDataSetChanged();
    }


    // Create new views (invoked by the layout manager)
    @Override
    public FeedAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.feed_card_1col, parent, false);
        // set the view's size, margins, paddings and layout parameters

        return new ViewHolder(v);
    }


    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        // Loop through each column to be displayed
        for (int i=0; i< holder.mTitle.length; i++) {
            final int dataIndex = (mColCount * position) + i;

            if (dataIndex < mDataset.size()) {
                HashMap<String, String> newsItem = mDataset.get((mColCount * position) + i);

                holder.mTitle[i].setText(newsItem.get(RSSGrabber.ITEM_TITLE));
                holder.mDescription[i].setText(newsItem.get(RSSGrabber.ITEM_DESC));
                holder.mPubDate[i].setText(newsItem.get(RSSGrabber.ITEM_PUBLISHED));

                final int index = i;
                // Image:
                Picasso.with(mContext)
                        .load(newsItem.get(RSSGrabber.ITEM_IMAGE + ":144x81"))
                        .into(holder.mImageView[index], new Callback() {
                            @Override
                            public void onSuccess() {
//                                Palette palette = Palette.generate(((BitmapDrawable)holder.mImageView[index].getDrawable()).getBitmap());
//                                Palette.Swatch vibrant = palette.getVibrantSwatch();
//                                if (vibrant != null)
//                                         holder.mTitle[index].setTextColor(vibrant.getRgb());
                                Palette.generateAsync(((BitmapDrawable) holder.mImageView[index].getDrawable()).getBitmap(),
                                        new Palette.PaletteAsyncListener() {
                                            @Override
                                            public void onGenerated(Palette palette) {
                                                Palette.Swatch vibrant = palette.getVibrantSwatch();
                                                if (vibrant != null) {
                                                    holder.mTitle[index].setTextColor(vibrant.getRgb());
                                                }
                                            }
                                        });
                            }

                            @Override
                            public void onError() {

                            }
                        }); // 1 line! This library is great!

                // The recycled view may previously have been set to be invisible, so make sure it's shown:
                ((View) holder.mTitle[i].getParent()).setVisibility(View.VISIBLE);

                // Attach a click handler to the card:
                View card = (View) holder.mTitle[i].getParent();
                card.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        cardClicked(dataIndex);
                    }
                });

            }
            else { // No content for this card - hide it!
                ((View)holder.mTitle[i].getParent()).setVisibility(View.GONE);
            }
        }
    }


    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
      return (int) Math.ceil(mDataset.size() / (double)mColCount);
    }


    private void cardClicked(int dataIndex) {
        ((MainActivity) mContext).loadStory(mDataset.get(dataIndex).get(RSSGrabber.ITEM_LINK));
    }


    // Provide a reference to the type of views that you are using
    // (custom viewholder)
    public static class ViewHolder extends RecyclerView.ViewHolder {

        public TextView mTitle[],
                mDescription[],
                mPubDate[];
        public ImageView mImageView[];

        public ViewHolder(View v) {
            super(v);

            // Deal with expensive findViewById() etc calls here, so they're only run on construction of the viewHolders
            ArrayList<View> cardContainers = new ArrayList<View>();
            v.findViewsWithText(cardContainers,"CARD_CONTAINER", View.FIND_VIEWS_WITH_CONTENT_DESCRIPTION);

            int colCount = cardContainers.size();
            mTitle = new TextView[colCount];
            mDescription = new TextView[colCount];
            mPubDate = new TextView[colCount];
            mImageView = new ImageView[colCount];

            for (int i=0; i < colCount; i++ ) {
                View card = cardContainers.get(i);
                mTitle[i] = (TextView) card.findViewById(R.id.card_title);
                mDescription[i] = (TextView) card.findViewById(R.id.card_description);
                mPubDate[i] = (TextView) card.findViewById(R.id.card_pub_date);
                mImageView[i] = (ImageView) card.findViewById(R.id.card_image);
            }
        }
    }
}
